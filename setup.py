from setuptools import setup

setup(
    name='chk',
    version='0.1.0',

    description='A tool for following development checklists',
    long_description=open('README.md').read(),

    author='Sam Clements',
    author_email='sam@borntyping.co.uk',
    url='https://github.com/borntyping/chk',
    license='MIT License',

    packages=[
        'chk'
    ],

    install_requires=[
        'click',
        'pyyaml'
    ],

    entry_points={
        'console_scripts': [
            'chk = chk.cli:main'
        ]
    },

    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
    ]
)
