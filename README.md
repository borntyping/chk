# chk [![](https://img.shields.io/pypi/v/chk.svg)](https://warehouse.python.org/project/chk/) [![](https://img.shields.io/pypi/l/chk.svg)](https://warehouse.python.org/project/chk/) [![](https://img.shields.io/travis/borntyping/chk/master.svg)](https://travis-ci.org/borntyping/chk)

A tool for following development checklists.

* [Source on GitHub](https://github.com/borntyping/chk)
* [Packages on PyPI](https://warehouse.python.org/project/chk/)
* [Builds on Travis CI](https://travis-ci.org/borntyping/chk)

Example
-------

This example could be used to release `chk`. It ensures the tests have been run
(and are passing), reminds the user to update the version numbers in the
package, asks the user for the version and creates a git tag with it, and then
uploads the release.

```yaml
- desc: Run the tests
  command: tox

- desc: Update the version number in setup.py

- desc: Update the version number in chk/__init__.py

- desc: Create a git tag for the current version
  command: "git tag -a 'v{version}' -m 'Version {version}'"
  args: ['version']

- desc: Upload the release to PyPI
  command: python setup.py sdist bdist_wheel upload
```

Installation
------------

```bash
pip3 install chk
```

Usage
-----

```bash
chk [-y] [.chk.yml]
```

See `chk --help` for more information.

Licence
-------

The MIT License (MIT)

Copyright (c) 2015 Sam Clements <sam@borntyping.co.uk>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
