"""Wrappers around click."""

import sys

import click


def confirm(*args, **kwargs):
    """Wrap `click.confirm` to output a newline if there is no TTY."""
    result = click.confirm(*args, **kwargs)
    if sys.stdout.isatty():
        click.echo()
    return result
