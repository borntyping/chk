"""Checklist implementation."""

import inspect
import subprocess
import sys

import click

import chk.click


class Item:
    """Superclass for checklist items."""

    @staticmethod
    def load(config):
        item_type = config.pop('type', None)
        if item_type == 'command' or config.get('command', None):
            return Command(**config)
        return Confirm(**config)

    def __init__(self, name=None):
        self.name = name

    def description(self):
        return self.name

    def ready(self, assume_yes=False):
        """Prompts the user to confirm they are ready to continue."""
        if assume_yes:
            click.echo("  Assuming the user would input yes, continuing...")
        else:
            chk.click.confirm("  Are you ready to continue?", abort=True)


class Command(Item):
    """
    A checklist item that executes a command.

    If manual mode is set, asks the user to confirm they ran the command
    instead.
    """

    def __init__(self, command, args=[], **kwargs):
        super().__init__(**kwargs)
        self.command = command
        self.args = args

    def perform(self, assume_yes, manual, logfile, logfile_path):
        click.secho('  $ {}'.format(self.command), fg='blue', bold=True)

        # Wait for the user to run the command and return early.
        if manual:
            self.ready(assume_yes)
            return

        # Run the command and log output to the logfile.
        try:
            subprocess.check_call(self.command, stdin=logfile, stdout=logfile)
        except subprocess.CalledProcessError as e:
            click.secho('  ! {}, see \'{}\' for more information.'.format(
                e, logfile_path), fg='red')
            sys.exit()


class Confirm(Item):
    """A checklist item that asks the user to confirm before continuing."""

    def perform(self, assume_yes):
        self.ready(assume_yes)


class Checklist:
    """A runnable checklist."""

    @classmethod
    def load(cls, config, **kwargs):
        return cls(items=map(Item.load, config), **kwargs)

    def __init__(self, items, assume_yes, logfile_path, manual):
        self.items = items
        self.assume_yes = assume_yes
        self.logfile_path = logfile_path
        self.manual = manual

    def run(self):
        with click.open_file(self.logfile_path, 'w') as logfile:
            for item in self.items:
                click.secho('- {}'.format(item.description()), fg='blue')
                kwargs = {}
                argspec = inspect.getargspec(item.perform)
                if 'assume_yes' in argspec.args:
                    kwargs['assume_yes'] = self.assume_yes
                if 'logfile' in argspec.args:
                    kwargs['logfile'] = logfile
                if 'logfile_path' in argspec.args:
                    kwargs['logfile_path'] = self.logfile_path
                if 'manual' in argspec.args:
                    kwargs['manual'] = self.manual
                item.perform(**kwargs)
