"""CLI entry point."""

import click
import yaml

import chk
import chk.checklist


@click.command()
@click.help_option('-h', '--help')
@click.version_option(chk.__version__, '-v', '--version')
@click.option(
    '-m', '--manual', is_flag=True,
    help='Run commands manually.')
@click.option(
    '-y', '--yes', 'assume_yes', is_flag=True,
    help='Assume yes for all yes/no questions.')
@click.option(
    '-l', '--logfile', 'logfile_path',
    type=click.Path(writable=True), default='.chk.log',
    help='Command output is written to this file.')
@click.argument(
    'configfile', type=click.File('r'), default='.chk.yml')
def main(configfile, **kwargs):
    """Create and run a checklist."""
    config = yaml.load(configfile.read())
    checklist = chk.checklist.Checklist.load(config, **kwargs)
    checklist.run()
